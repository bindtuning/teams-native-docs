Ready to install your BindTuning native Teams apps on Microsoft Teams? Follow our step-by-step instructions.
If you require assistance at any time, please contact us at <support@bindtuning.com>.
<br>  
For more details about BindTuning's Microsoft Teams Add-on visit <https://bindtuning.com/teams>.