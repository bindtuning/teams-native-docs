### Add BindTuning native Teams app

This proceed can be done within 2 diferent approachs:
- Add to Team
- Add to Tab

___

### Add to Team

1. Click on the default **Apps** button

1. On the search field type the name of the app which you want to add

1. On the result show up with the desired app, click on it

1. Ckick on the button **Add to a team**
![available-pool.png](../images/add_app_1.1.PNG)

1. Type de name of the channel you wanbt to add the app to, and click **Set up a tab**
![available-pool.png](../images/add_app_1.2.PNG)


### Add to Tab

1. Navigate to the channel you want to add the app in

1. Click on the default **+** signal

1. On the search field type the name of the app which you want to add

1. On the result show up with the desired app, click on it

1. Ckick on the button **Add**
![available-pool.png](../images/add_app_1.PNG)

___
### Get started

After adding your BindTuning native Teams app, you'll be given the optionto take a quick tour, to learn how to use the product or skip it completely.

![available-pool.png](../images/add_app_2.PNG)