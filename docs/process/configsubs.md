Ready to start using your BindTuning native Teams apps for Microsoft Teams?


1. Sign in to your BindTuning account;
![available-pool.png](../images/add_app_3.PNG)

1. Pick the subscription to use.
![available-pool.png](../images/add_app_4.PNG)

1. Configure de app settings

